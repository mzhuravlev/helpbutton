-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Апр 06 2013 г., 18:43
-- Версия сервера: 5.1.63
-- Версия PHP: 5.3.3-7+squeeze14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- База данных: `hebun5468`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cms_monitoring_object`
--

CREATE TABLE IF NOT EXISTS `cms_monitoring_object` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `mark_color` char(7) NOT NULL,
  `title` varchar(100) NOT NULL,
  `imei` varchar(15) NOT NULL,
  `is_enabled` int(1) NOT NULL,
  `is_track` int(1) NOT NULL,
  `track_days` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `imei` (`imei`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дамп данных таблицы `cms_monitoring_object`
--


-- --------------------------------------------------------

--
-- Структура таблицы `HB_ALARM_MESSAGE`
--

CREATE TABLE IF NOT EXISTS `HB_ALARM_MESSAGE` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imei` varchar(15) NOT NULL,
  `dispatchDate` datetime NOT NULL,
  `incomeDate` datetime NOT NULL,
  `alarmType` int(11) NOT NULL,
  `additionalInfo` varchar(255) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Структура таблицы `HB_TRACKER_MESSAGE`
--

CREATE TABLE IF NOT EXISTS `HB_TRACKER_MESSAGE` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `imei` varchar(15) NOT NULL,
  `dispatchDate` datetime NOT NULL,
  `incomeDate` datetime NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  `additionalInfo` varchar(255) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;



CREATE TABLE `HB_CURRENT_LOCATION` (
`imei` VARCHAR( 15 ) NOT NULL ,
`dispatchDate` DATETIME NOT NULL ,
`incomeDate` DATETIME NOT NULL ,
`longitude` DOUBLE NOT NULL ,
`latitude` DOUBLE NOT NULL ,
PRIMARY KEY (  `imei` )
) ENGINE = MYISAM ;
