CREATE  TABLE `hebun5468`.`HB_SMS` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `message` VARCHAR(8000) NOT NULL ,
  `phone` VARCHAR(45) NOT NULL ,
  `externalID` VARCHAR(45) NOT NULL ,
  `dateStamp` DATETIME NOT NULL ,
  `sendDate` DATETIME NULL ,
  `status` INT NOT NULL ,
  PRIMARY KEY (`id`) );