package hb.sms;

/**
 * Created with IntelliJ IDEA.
 * User: Леонид
 * Date: 06.04.13
 * Time: 20:14
 */
public enum SmsStatus {

    OK(100),
    WrongKey(200),
    NotEnoughMoney(201),
    WrongReceiver(202),
    NoMassageBody(203),
    SenderNameNotAllowed(204),
    MessageTooLong(205),
    LimitExceed(206),
    WrongNumber(207),
    WrongTime(208),
    NumberBlocked(209),
    NeedPost(210),
    MethodNotFound(211),
    ServiceNotAvailable(220),
    Over100Messages(230),
    WrongToken(300),
    WrongPassword(301),
    NotApproved(302);

    private int code;

    SmsStatus(int code){
        this.code = code;
    }

    public static SmsStatus fromString(String code) {
        if(code.length() != 3)
            throw new IllegalArgumentException("String length should be 3");
        int intCode = Integer.parseInt(code);

        for (SmsStatus status : SmsStatus.values()) {
            if (intCode == status.code) {
                return status;
            }
        }
        throw new IllegalArgumentException("Corresponding enum not found!");
    }

    public String getText(){
        switch (this) {
            case LimitExceed:
                return ("Будет превышен или уже превышен дневной лимит на отправку сообщений");
            case MessageTooLong:
                return ("Сообщение слишком длинное (превышает 8 СМС)");
            case MethodNotFound:
                return ("Метод не найден");
            case NeedPost:
                return ("Используется GET, где необходимо использовать POST");
            case NoMassageBody:
                return ("Нет текста сообщения");
            case OK:
                return ("Сообщение принято к отправке");
            case WrongKey:
                return ("Неправильный api_id");
            case NotEnoughMoney:
                return ("Не хватает средств на лицевом счету");
            case WrongReceiver:
                return ("Неправильно указан получатель");
            case SenderNameNotAllowed:
                return ("Имя отправителя не согласовано с администрацией");
            case WrongNumber:
                return ("На этот номер (или один из номеров) нельзя отправлять сообщения, либо указано более 100 номеров в списке получателей");
            case WrongTime:
                return ("Параметр time указан неправильно");
            case NumberBlocked:
                return ("Вы добавили этот номер (или один из номеров) в стоп-лист");
            case ServiceNotAvailable:
                return ("Сервис временно недоступен, попробуйте чуть позже.");
            case Over100Messages:
                return ("Сообщение не принято к отправке, так как на один номер в день нельзя отправлять более 100 сообщений.");
            case WrongToken:
                return ("Неправильный token (возможно истек срок действия, либо ваш IP изменился)");
            case WrongPassword:
                return ("Неправильный пароль, либо пользователь не найден");
            case NotApproved:
                return ("Пользователь авторизован, но аккаунт не подтвержден (пользователь не ввел код, присланный в регистрационной смс)");
        }
        return "";
    }

}
