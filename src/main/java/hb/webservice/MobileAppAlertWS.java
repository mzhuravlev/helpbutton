package hb.webservice;

import java.util.Date;

/**
 * User: mzhuravlev
 * Date: 26.03.13
 * Time: 23:22
 */
public interface MobileAppAlertWS {

  public void trackerAlert(String imei, Double longitude, Double latitude, Date dispatchDate, String additionalInfo);

  public void alarmAlert(String imei, Date dispatchDate, Integer alarmType, String additionalInfo);

  public  void sendSms(String message, String phone);
}
