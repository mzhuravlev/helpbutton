package hb.webservice;

import hb.common.BusinessException;
import hb.entity.AlarmMessage;
import hb.entity.CurrentLocation;
import hb.entity.TrackerMessage;
import hb.service.SmsService;
import hb.service.api.MobileMessageService;
import org.apache.cxf.annotations.WSDLDocumentation;
import org.apache.cxf.annotations.WSDLDocumentationCollection;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.io.IOException;
import java.util.Date;

/**
 * User: mzhuravlev
 * Date: 26.03.13
 * Time: 23:21
 */
@WebService(name = "MobileAppAlertWS", targetNamespace = "hb.service", serviceName = "MobileAppAlertWS")
@WSDLDocumentationCollection(
  {
    @WSDLDocumentation(value = "Web-Сервис для получения сообщений от мобильного приложения.",
      placement = WSDLDocumentation.Placement.TOP)
  }
)
@SOAPBinding(
  style = SOAPBinding.Style.RPC,
  use = SOAPBinding.Use.LITERAL,
  parameterStyle = SOAPBinding.ParameterStyle.BARE
)
public class MobileAppAlertWSImpl implements MobileAppAlertWS {

  @Autowired
  public MobileMessageService mobileMessageService;

    @Autowired
    public SmsService smsService;

  @Override
  @WebMethod
  public void trackerAlert(
    @WebParam(name = "imei") String imei,
    @WebParam(name = "longitude")Double longitude,
    @WebParam(name = "latitude")Double latitude,
    @WebParam(name = "dispatchDate") Date dispatchDate,
    @WebParam(name = "additionalInfo") String additionalInfo)
  {
    Date incomeDate = new Date();
    TrackerMessage trackerMessage = new TrackerMessage(imei,dispatchDate,incomeDate,longitude,latitude,additionalInfo);
    CurrentLocation currentLocation = new CurrentLocation(imei,dispatchDate,incomeDate,longitude,latitude);
    mobileMessageService.saveTrackerAlert(trackerMessage);
      mobileMessageService.saveCurrentLocation(currentLocation);
    System.out.println("MobileAppAlertWSImpl.trackerAlert: " + trackerMessage);
  }

  @Override
  @WebMethod
  public void alarmAlert(
    @WebParam(name = "imei") String imei,
    @WebParam(name = "dispatchDate") Date dispatchDate,
    @WebParam(name = "alarmType")Integer alarmType,
    @WebParam(name = "additionalInfo") String additionalInfo)
  {
    AlarmMessage alarmMessage = new AlarmMessage();
    alarmMessage.setImei(imei);
    alarmMessage.setDispatchDate(dispatchDate);
    alarmMessage.setIncomeDate(new Date());
    alarmMessage.setAlarmType(alarmType);
    alarmMessage.setAdditionalInfo(additionalInfo);
    System.out.println("MobileAppAlertWSImpl.alarmAlert: " + alarmMessage);
    mobileMessageService.saveAlarmAlert(alarmMessage);
  }

    @Override
    @WebMethod
    public void sendSms(@WebParam(name = "message") String message,@WebParam(name = "phone") String phone) {
        try {
            smsService.SendMessage(message, phone);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (BusinessException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
