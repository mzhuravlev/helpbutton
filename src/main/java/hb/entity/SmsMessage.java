package hb.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Леонид
 * Date: 06.04.13
 * Time: 21:33
 */
@Entity
@Table(name = "HB_SMS")
public class SmsMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String message;
    private String phone;
    private String externalID;
    private Date dateStamp;
    private Date sendDate;

    public SmsMessage(String message, String phone, String externalID, Date dateStamp, Date sendDate, int status) {
        this.message = message;
        this.phone = phone;
        this.externalID = externalID;
        this.dateStamp = dateStamp;
        this.sendDate = sendDate;
        this.status = status;
    }

    public Date getSendDate() {

        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    private int status;


    public SmsMessage() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getExternalID() {
        return externalID;
    }

    public void setExternalID(String externalID) {
        this.externalID = externalID;
    }

    public Date getDateStamp() {
        return dateStamp;
    }

    public void setDateStamp(Date dateStamp) {
        this.dateStamp = dateStamp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
