package hb.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * User: mzhuravle
 * Date: 27.03.13
 * Time: 1:11
 */

@Entity
@Table(name = "HB_TRACKER_MESSAGE")
public class TrackerMessage {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private String imei;
  private Date dispatchDate;
  private Date incomeDate;
  private Double longitude;
  private Double latitude;
  private String additionalInfo;

  public TrackerMessage() {
  }

  public TrackerMessage(String imei, Date dispatchDate, Date incomeDate, Double longitude, Double latitude, String additionalInfo) {
    this.imei = imei;
    this.dispatchDate = dispatchDate;
    this.incomeDate = incomeDate;
    this.longitude = longitude;
    this.latitude = latitude;
    this.additionalInfo = additionalInfo;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Double getLongitude() {
    return longitude;
  }

  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

  public Double getLatitude() {
    return latitude;
  }

  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }

  public Date getDispatchDate() {
    return dispatchDate;
  }

  public void setDispatchDate(Date dispatchDate) {
    this.dispatchDate = dispatchDate;
  }

  public Date getIncomeDate() {
    return incomeDate;
  }

  public void setIncomeDate(Date incomeDate) {
    this.incomeDate = incomeDate;
  }

  public String getAdditionalInfo() {
    return additionalInfo;
  }

  public void setAdditionalInfo(String additionalInfo) {
    this.additionalInfo = additionalInfo;
  }

  public String getImei() {
    return imei;
  }

  public void setImei(String imei) {
    this.imei = imei;
  }
}
