package hb.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * User: mzhuravle
 * Date: 27.03.13
 * Time: 1:12
 */
@Entity
@Table(name = "HB_ALARM_MESSAGE")
public class AlarmMessage {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private String imei;
  private Date dispatchDate;
  private Date incomeDate;
  private Integer alarmType;
  private String additionalInfo;


  public AlarmMessage() {
  }

  public AlarmMessage( String imei, Date dispatchDate, Date incomeDate, Integer alarmType, String additionalInfo) {
    this.imei = imei;
    this.dispatchDate = dispatchDate;
    this.incomeDate = incomeDate;
    this.alarmType = alarmType;
    this.additionalInfo = additionalInfo;
  }

  public Date getDispatchDate() {
    return dispatchDate;
  }

  public void setDispatchDate(Date dispatchDate) {
    this.dispatchDate = dispatchDate;
  }

  public Date getIncomeDate() {
    return incomeDate;
  }

  public void setIncomeDate(Date incomeDate) {
    this.incomeDate = incomeDate;
  }

  public Integer getAlarmType() {
    return alarmType;
  }

  public void setAlarmType(Integer alarmType) {
    this.alarmType = alarmType;
  }

  public String getAdditionalInfo() {
    return additionalInfo;
  }

  public void setAdditionalInfo(String additionalInfo) {
    this.additionalInfo = additionalInfo;
  }

  public String getImei() {
    return imei;
  }

  public void setImei(String imei) {
    this.imei = imei;
  }
}
