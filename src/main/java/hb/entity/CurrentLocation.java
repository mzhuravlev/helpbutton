package hb.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * User: mzhuravle
 * Date: 06.04.13
 * Time: 19:10
 */
@Entity
@Table(name = "HB_CURRENT_LOCATION")
public class CurrentLocation {

  @Id
  private String imei;
  private Date dispatchDate;
  private Date incomeDate;
  private Double longitude;
  private Double latitude;

  public CurrentLocation() {
  }

  public CurrentLocation(String imei, Date dispatchDate, Date incomeDate, Double longitude, Double latitude) {
    this.imei = imei;
    this.dispatchDate = dispatchDate;
    this.incomeDate = incomeDate;
    this.longitude = longitude;
    this.latitude = latitude;
  }

  public String getImei() {
    return imei;
  }

  public void setImei(String imei) {
    this.imei = imei;
  }

  public Date getDispatchDate() {
    return dispatchDate;
  }

  public void setDispatchDate(Date dispatchDate) {
    this.dispatchDate = dispatchDate;
  }

  public Date getIncomeDate() {
    return incomeDate;
  }

  public void setIncomeDate(Date incomeDate) {
    this.incomeDate = incomeDate;
  }

  public Double getLongitude() {
    return longitude;
  }

  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

  public Double getLatitude() {
    return latitude;
  }

  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }
}
