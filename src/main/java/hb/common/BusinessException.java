package hb.common;

/**
 * Created with IntelliJ IDEA.
 * User: Леонид
 * Date: 06.04.13
 * Time: 22:16
 */
public class BusinessException extends Exception {
    public BusinessException(String s) {
        super(s);
    }
}
