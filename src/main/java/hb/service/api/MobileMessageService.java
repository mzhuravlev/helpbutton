package hb.service.api;

import hb.entity.AlarmMessage;
import hb.entity.CurrentLocation;
import hb.entity.TrackerMessage;

/**
 * User: mzhuravle
 * Date: 27.03.13
 * Time: 1:04
 */
public interface MobileMessageService {

  public boolean saveTrackerAlert(TrackerMessage trackerMessage);

  public boolean saveAlarmAlert (AlarmMessage alarmMessage);

  public boolean saveCurrentLocation(CurrentLocation currentLocation);
}
