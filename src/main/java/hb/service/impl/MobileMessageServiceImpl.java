package hb.service.impl;

import hb.entity.AlarmMessage;
import hb.entity.CurrentLocation;
import hb.entity.TrackerMessage;
import hb.service.api.MobileMessageService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * User: mzhuravle
 * Date: 27.03.13
 * Time: 1:06
 */
@Service("mobileMessageService")
public class MobileMessageServiceImpl implements MobileMessageService {

  private EntityManager entityManager;

  @PersistenceContext
  public void setEntityManager(EntityManager entityManager) {
    this.entityManager = entityManager;
  }

  @Override
  @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
  public boolean saveTrackerAlert(TrackerMessage trackerMessage) {
    System.out.println("MobileMessageService.trackerAlert");
    entityManager.persist(trackerMessage);
    entityManager.flush();
    return true;
  }

  @Override
  @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
  public boolean saveAlarmAlert(AlarmMessage alarmMessage) {
    entityManager.persist(alarmMessage);
    entityManager.flush();
    return true;
  }

  @Override
  @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
  public boolean saveCurrentLocation(CurrentLocation currentLocation) {
    entityManager.merge(currentLocation);
    return true;
  }


}
