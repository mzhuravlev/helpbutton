package hb.service.impl;

import hb.common.BusinessException;
import hb.entity.SmsMessage;
import hb.service.SmsService;
import hb.sms.SmsStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Леонид
 * Date: 06.04.13
 * Time: 21:52
 */
@Service("smsService")
public class SmsServiceImpl implements SmsService {

    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional(readOnly=false, propagation= Propagation.REQUIRED)
    public void SendMessage(String message, String phone) throws IOException, BusinessException {
        String url = "http://sms.ru/sms/send";
        String charset = "UTF-8";
        // если тест, то 1
        String test = "0";
        String api_id = "b94d1785-1dd1-e164-b179-b290275a6fbb";
        String query = String.format("to=%s&text=%s&test=%s&api_id=%s",
                URLEncoder.encode(phone, charset),
                URLEncoder.encode(message, charset),
                URLEncoder.encode(test, charset),
                URLEncoder.encode(api_id, charset));
        URLConnection connection = new URL(url + "?" + query).openConnection();
        connection.setRequestProperty("Accept-Charset", charset);
        InputStream response = connection.getInputStream();
        String resp = convertStreamToString(response);
        String[] result = resp.split("\n");
        SmsStatus status = SmsStatus.fromString(result[0]);
        if(status != SmsStatus.OK)
            throw new BusinessException(status.getText());
        SmsMessage sms = new SmsMessage();
        sms.setMessage(message);
        sms.setPhone(phone);
        sms.setDateStamp(new Date());
        sms.setExternalID(result[1]);
        /**
         * 1 - отправлено
         * 2 - обрабатывается
         * 3 - ошибка
         */
        sms.setStatus(2);
        entityManager.persist(sms);
        entityManager.flush();
    }

    public static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

}
