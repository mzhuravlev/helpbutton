package hb.service;

import hb.common.BusinessException;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Леонид
 * Date: 06.04.13
 * Time: 21:33
 */
public interface SmsService {
    public void SendMessage(String message, String phone) throws IOException, BusinessException;
}
